var cheerio = require('cheerio');
var request = require('request');
var fs = require('fs');
var json2csv = require('json2csv');

var translations = [];

//crawlPage('/slownictwo/slownictwo-angielskie-poziom-b1/page/32', function(){
crawlPage('/slownictwo/slownictwo-angielskie-poziom-b1', saveTranslations);

function crawlPage(url, callback) {
    url = 'http://www.ang.pl' + url;
    console.log('Crawl: ' + url);
    request(url, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            $ = cheerio.load(body);
            $wordBoxes = $('.large-9.medium-9.columns').find('div.row.mtop');
            $wordBoxes.each(function (i, wordBox) {
                var englishMeaning = $(wordBox).find('.end .row p a.sm2_button').text();
                var polishMeaning = $(wordBox).find('.end .row .end p em').text();
                addWord(englishMeaning, polishMeaning);
            });

            //console.log(translations);
            // if next page exists
            var nextPageLink = $('body').find('.row.mtop2 .row.panel.mbot a[rel="next"]').attr('href');
            if (nextPageLink !== undefined) {
                crawlPage(nextPageLink, callback);
            } else {
                callback();
            }
        }
    });
}

function addWord(englishMeaning, polishMeaning) {
    translations.push({
        'en': englishMeaning,
        'pl': polishMeaning
    });
}

function saveTranslations() {
    json2csv({ data: translations, fields: ['en', 'pl'] }, function(err, csv) {
        if (err) console.log(err);
        fs.writeFile('slownictwo-angielskie-poziom-b1.csv', csv, function(err) {
            if (err) throw err;
            console.log('file saved');
        });
    });
}